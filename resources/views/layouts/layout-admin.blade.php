<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
     <title>
        @yield('page-title') - 
            
        @if(trim($__env->yieldContent('page-title')) && Auth::user()->gard == 'admin')
            {{ config('app.name', 'Newsletters') }}
        
        @endif
    </title>



  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('public/vendor/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
 
  <!-- Theme style -->

    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="{{ asset('public/metronic/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/metronic/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/metronic/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('public/assets/css/iziToast.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('public/vendor/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Custom css -->
    <link rel="stylesheet" href="{{ asset('public/vendor/dist/css/custom.css') }}">

    <!--end::Global Theme Styles-->
    <!--begin::Layout Themes(used by all pages)-->
    <link href="{{ asset('public/metronic/css/themes/layout/header/base/light.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/metronic/css/themes/layout/header/menu/light.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/metronic/css/themes/layout/brand/dark.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/metronic/css/themes/layout/aside/dark.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Layout Themes-->
    

  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('public/vendor/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('public/vendor/plugins/summernote/summernote-bs4.min.css') }}">
 <!-- Select -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />



<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('public/metronic/plugins/custom/datatables/datatables.bundle.css') }}">
  <link rel="stylesheet" href="{{ asset('public/vendor/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/vendor/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">




  @stack('style')
</head>
<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
 

  <!-- base page -->
   @include('layouts.base._layout')

<div id="commanModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modelCommanModelLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <h4 class="modal-title" id="modelCommanModelLabel"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>

 
 
<!-- ./wrapper -->

<!--begin::Global Config(global config for global JS scripts)-->
  <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
    <!--end::Global Config-->
    <!--begin::Global Theme Bundle(used by all pages)-->
    <script src="{{ asset('public/metronic/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('public/metronic/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
    <script src="{{ asset('public/metronic/js/scripts.bundle.js') }}"></script>
    <!--end::Global Theme Bundle-->
    <!--end::Page Vendors-->
    <!--begin::Page Scripts(used by this page)-->
    <script src="{{ asset('public/metronic/js/pages/widgets.js') }}"></script>
    <!--end::Page Scripts-->


<!-- jQuery -->
<script src="{{ asset('public/vendor/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('public/vendor/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('public/vendor/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<script src="{{ asset('public/vendor/plugins/moment/moment.min.js') }}"></script>
 
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('public/vendor/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('public/vendor/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('public/vendor/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
 
<script src="{{ asset('public/vendor/dist/js/scrollreveal.min.js') }}"></script>
<script src="{{ asset('public/vendor/dist/js/iziToast.min.js') }}"></script>
 
<!-- DataTables  & Plugins -->
<script src="{{ asset('public/metronic/plugins/custom/datatables/datatables.bundle.js') }}"></script>

<script src="{{ asset('public/vendor/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/vendor/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('public/vendor/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/vendor/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/vendor/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/vendor/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('public/vendor/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/vendor/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/vendor/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/vendor/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{ asset('public/vendor/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

 <script src="{{ asset('public/app/custom.js') }}" defer></script>

 <script>
  $(function () {
    $("#datatable, #datatable2").DataTable({
       responsive: true,
      "dom": 'lfBrtip',
      "columnDefs": [ {
          "targets"  : 'no-sort',
      "orderable": false,
      "order": []
     } ],
      "buttons": [{
            extend: 'colvis',
            text: "Colonnes"
        }, {
                extend: 'collection',
                text: "Export",
                buttons: [
                 "csv", "excel", "pdf", "print"
                ]
            }],
      "language": {
                    "lengthMenu": "Afficher _MENU_ résulats par page",
                    "zeroRecords": "Aucun résultat",
                    "info": "Page _PAGE_ sur _PAGES_",
                    "infoEmpty": "Aucun résultat",
                    "infoFiltered": "",
                    "sSearch": "<span class='ion-search'></span> Recherche : ",
                    searchPlaceholder: "Recherche",
                    oPaginate: {
                        sFirst: "Première",
                        sLast: "Dernière",
                        sNext: "Suivant",
                        sPrevious: "Précédent"
                    },
                },

    }).buttons().container().appendTo('#datatable_wrapper .col-md-6:eq(0)');
   
  });
</script>
 
@stack('scripts')
@if ($message = Session::get('success'))
    <script>toastr('{{__('Success')}}', '{!! $message !!}', 'success')</script>
@endif

@if ($message = Session::get('error'))
    <script>toastr('{{__('Error')}}', '{!! $message !!}', 'error')</script>
@endif

@if ($message = Session::get('info'))
    <script>toastr('{{__('Info')}}', '{!! $message !!}', 'info')</script>
@endif
@if ($message = Session::get('warning'))
    <script>toastr('{{__('Warning')}}', '{!! $message !!}', 'warning')</script>
@endif
</body>
</html>
