<style>
    .disableClick {pointer-events: none;}
</style>
   <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link brand-link-custom">
      <img src="{{asset('/public/storage/logo/logo-full.png')}}" alt=" " class="brand-image img-logo "  >
    </a>   <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <!-- select client Form -->
      <div class="form-inline full-with">
        <div class="input-group" data-widget="sidebar-search">
          <select class="form-control form-control-sidebar full-with" onChange="getInfosClient(this.value)">
            <option value="">Sélection du client</option>
            @foreach($data_customer_layouts as $val)
              <option value="{{$val->slug}}" 
                {{(Session::has('selected_client_slug') && Session::get('selected_client_slug') == $val->slug) ? 'selected':''}}
              >{{$val->lname.' '.$val->fname}} / {{$val->slug}}</option>
            @endforeach
          </select>
        </div>
      </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('admin.home') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>{{ __('Dashbord')}}</p>
            </a>
          </li>
        
          <li class="nav-item (Session::has('selected_client_slug') && Session::get('selected_client_slug') != '') ? '':'disableClick'">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                 {{__('Clients')}}
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right"></span>
              </p>
            </a>
            @if(Session::has('selected_client_slug'))
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin.client.infos.client', Session::get('selected_client_slug'))}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('Infos client')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.client.licences', Session::get('selected_client_slug'))}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('Licences')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.client.roles', Session::get('selected_client_slug'))}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('Rôles personnalisés')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.client.theme', Session::get('selected_client_slug'))}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('Thèmes')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.client.filtres', Session::get('selected_client_slug'))}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('Filtres')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.client.connecteurs', Session::get('selected_client_slug'))}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('Connecteurs')}}</p>
                </a>
              </li>
            </ul>
            @endif
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
               {{ __('Paramètres global')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.client.list') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{ __('Clients')}}</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('admin.site.list') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{ __('Sites')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.role.list') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{ __('Rôles')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.utilisateur.list') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{ __('Utilisateurs')}}</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('admin.permission.list') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{ __('Permissions')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.smtp.list') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{ __('Smtp')}}</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                {{ __('Documentation')}}
               
              </p>
            </a>
          </li>
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  <script>
  function getInfosClient(slug){
      $.ajax({
            url:"{{ URL::to('/') }}/admin/client/storeSlug/"+slug,
            type: "GET",
        success: function(msg){
          if(slug != ''){
            window.location = "{{ URL::to('/') }}/admin/"+slug+"/infos-client"
          }
        }
      });
  }
  </script>