{{-- Topbar --}}
<div class="">
 

    {{-- User --}}
 
            <div class="">
                {{-- Toggle --}}
                <div class="" >
                    <a href="">
                    <div class="btn btn-icon w-auto btn-clean d-flex align-items-center btn-lg px-2">
                        <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">{{__('Bienvenue')}},</span>
                        <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{ Auth::user()->fname }}</span>
                        <span class="symbol symbol-35 symbol-light-success">
                            <span class="symbol-label font-size-h5 font-weight-bold">
                                
                                 @php

                                    echo strtoupper(substr(Auth::user()->fname, 0, 1));

                                 @endphp

                            </span>
                        </span>
                    </div>
                    </a>
                </div>

                {{-- Dropdown --}}
               
            </div>
         
</div>
