
 

    @include('layouts.base._header-mobile')

    <div class="d-flex flex-column flex-root">
        <div class="d-flex flex-row flex-column-fluid page" style="position: absolute;">
           
                @include('layouts.base._aside')

            
        
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper" style="padding-top: 0">

                @include('layouts.base._header')

                <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">

                  

                    @include('layouts.base._content')
                </div>

                @include('layouts.base._footer')
            </div>
        </div>
    </div>

 

@if (config('layouts.self.layouts') != 'blank')

 
@endif
