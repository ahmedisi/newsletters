{{-- Header --}}
<div id="kt_header" class="header  >

    {{-- Container --}}
    <div class="container-fluid d-flex align-items-center justify-content-between">
        
           

            {{-- Header Menu --}}
            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                <div id="kt_header_menu" class="header-menu header-menu-mobile  "  >
                    <ul class="menu-nav ">
                        <li class="menu-item " aria-haspopup="true">
                                      <a href="#"  class="menu-link "   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
             <span class="menu-text"> <i class="mdi mdi-logout mr-1"></i> {{ __('Déconnexion') }}</span>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        

                    </ul>
                </div>
            </div>
 

        @include('layouts.partials.extras._topbar')
    </div>
</div>
