{{-- Content --}}
@if (config('layouts.content.extended'))
    @yield('content')
@else
    <div class="d-flex flex-column-fluid">
        <div class="" style="    width: 100%;
    margin-left: 23%;
    margin-right: 10%;">
            @yield('content')
        </div>
    </div>
@endif
