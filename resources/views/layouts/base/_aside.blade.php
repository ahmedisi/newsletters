{{-- Aside --}}
 
<style>
    .disableClick {pointer-events: none;}
</style>
<div class="aside aside-left   d-flex flex-column flex-row-auto" id="kt_aside">

    {{-- Brand --}}
    <div class="brand flex-column-auto  " id="kt_brand">
        <div class="brand-logo">
            <a href="{{ url('/') }}">
                
                
            </a>
        </div>

      
            <button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
                 
            </button>
       

    </div>

    {{-- Aside menu --}}
    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">

        @if (config('layouts.aside.self.display') === false)
            <div class="header-logo">
                <a href="{{ url('/') }}">
                    <img alt="{{ config('app.name') }}" src="{{ asset('media/logos/'.$kt_logo_image) }}"/>
                </a>
            </div>
        @endif


    <div class="user-panel mt-3  d-flex">
          <!-- select client Form -->
      
      </div>

        <div
            id="kt_aside_menu"
            class="aside-menu my-4  "
            data-menu-vertical="1"
            >

            <ul class="menu-nav  ">
                <li class="menu-item menu-item-active" aria-haspopup="true">
                                    <a href=" " class="menu-link">
                                        
                                             
                                             <span class="menu-text">{{ __('Dashboard')}}</span>
                                        
                                    </a>
               </li>

 
 
                                        </ul>
                                    </div>
                                </li>
 


            </ul>
        </div>
    </div>

</div>
 