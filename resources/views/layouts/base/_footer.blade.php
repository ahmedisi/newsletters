{{-- Footer --}}

<div class="footer bg-white py-4 d-flex flex-lg-column  " id="kt_footer">
    {{-- Container --}}
    <div class="  d-flex flex-column flex-md-row align-items-center justify-content-between">
        {{-- Copyright --}}
        <div class="text-dark order-2 order-md-1">
            
            <a href="#" target="_blank" class="text-dark-75 text-hover-primary">Copyright © NIS <span class="text-muted font-weight-bold mr-2">{{ date("Y") }} </span> .</a>
        </div>

        {{-- Nav --}}
        <div class="nav nav-dark order-1 order-md-2">
             Version 1.0.0
        </div>
    </div>
</div>
