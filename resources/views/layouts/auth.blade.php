<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @yield('page-title') - {{ config('app.name', 'Newsletter') }}
    </title>

    <link rel="shortcut icon" href="{{asset(Storage::url('logo/favicon.png'))}}">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">

    <!-- App css -->
    <link href="{{ asset('public/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/css/components.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public/assets/css/iziToast.min.css') }}">
</head>
<body>

<div id="app">
    <section class="section">
        <div class="container mt-1">
            <?php
            $dir = base_path() . '/resources/lang/';
            $glob =  glob($dir."*",GLOB_ONLYDIR);
            $arrLang =  array_map(function($value) use($dir) { return str_replace($dir, '', $value); }, $glob);
            $arrLang =  array_map(function($value) use($dir) { return preg_replace('/[0-9]+/', '', $value); }, $arrLang);
            $arrLang = array_filter($arrLang);
            $currantLang = basename(App::getLocale());
            ?>
            <div class="row">
                <div class="col-12">
                    <select style="display: none" name="language" class="form-control pr-1 float-right" style="width: 70px;">
                        @foreach($arrLang as $lang)
                            <option value="{{$lang}}" @if($currantLang == $lang) selected @endif>{{Str::upper($lang)}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12 col-sm-8 offset-sm-1 col-md-6 offset-md-3 col-lg-6 offset-lg-1 col-xl-8 offset-xl-2">
                   
                    @if(session()->has('info'))
                        <div class="alert alert-primary">
                            {{ session()->get('info') }}
                        </div>
                    @endif
                    @if(session()->has('status'))
                        <div class="alert alert-info">
                            {{ session()->get('status') }}
                        </div>
                    @endif
                    @yield('content')
                    <div class="simple-footer">
                        {{env('FOOTER_TEXT')}}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="{{ asset('public/assets/js/stisla.js') }}"></script>

<!-- JS Libraies -->

<!-- Template JS File -->
<script src="{{ asset('public/assets/js/scrollreveal.min.js') }}"></script>
<script src="{{ asset('public/assets/js/scripts.js') }}"></script>
{{--<script src="{{ asset('public/assets/js/custom.js') }}"></script>--}}
<script src="{{ asset('public/vendor/dist/js/iziToast.min.js') }}"></script>

@stack('script')
<script>
    $(document).on("change","select[name='language']",function(){
        window.location.href = url+"/"+$(this).val();
    });
</script>
<!-- Page Specific JS File -->

@if ($message = Session::get('success'))
    <script>toastr('{{__('Success')}}', '{!! $message !!}', 'success')</script>
@endif

@if ($message = Session::get('error'))
    <script>toastr('{{__('Error')}}', '{!! $message !!}', 'error')</script>
@endif

@if ($message = Session::get('info'))
    <script>toastr('{{__('Info')}}', '{!! $message !!}', 'info')</script>
@endif
@if ($message = Session::get('warning'))
    <script>toastr('{{__('Warning')}}', '{!! $message !!}', 'warning')</script>
@endif

</body>
</html>
