@extends('layouts.auth')
@push('script')
    <script>
        var url = "{{route('login')}}";
    </script>
@endpush

@section('page-title') {{__('Connexion')}} @endsection

@section('content')
 
    <div class="card card-nis">
        <div class="card-header">
            <h4>{{ __('Connexion') }}</h4>
            
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <label for="emailaddress">{{ __('Adresse e-mail') }}</label>
                    <input class="form-control @error('email') is-invalid @enderror" type="email" name="email" id="emailaddress" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('Entrer votre Email') }}">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                         
                        <strong>{{$message}}</strong>
                    
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <a href="{{ route('password.request') }}" class="text-muted float-right"><small>{{ __('Mot de passe oublié?') }}</small></a>
                    <label for="password">{{ __('Mot de passe') }}</label>
                    <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" required autocomplete="current-password" id="password" placeholder="{{ __('Tapez votre mot de passe') }}">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group mb-3">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="checkbox-signin" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="custom-control-label" for="checkbox-signin">{{ __('Se souvenir de moi') }}</label>
                    </div>
                </div>
                <div class="form-group mb-0 text-center">
                    <button class="btn btn-nis btn-block btntextnis" type="submit"><i class="mdi mdi-login"></i> {{ __('Se connecter') }} </button>
                </div>
            </form>
        </div>
    </div>
  

@endsection
