@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Liste des newsletters</h1>

    @if ($message)
        {!! $message !!}
    @endif
    <br><br>
    <div class="row justify-content-center" style="width: 100%">

        <div class="col-md-12">
            @foreach ($newsletters as $val)
             
            <div class="card row">
                <div class="col-md-3">
                    <img src="">
                </div>
                <div class="col-md-9">
                     <h2>{{$val->titre}}</h2>
                     <p>{{$val->description}}</p>
                     <p>
                          <button type="button" class="btn btn-primary" id="subscribbtn" data="{{$val->id}}" data-toggle="modal" data-target="#newslettersModal">Je m'abonne</button>
                     </p>
                </div>
            </div>
            
            @endforeach
        </div>

        
    </div>
</div>

<div class="modal" id="newslettersModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
 
    <div class="modal-content">
    <form action="{{route('newsletter.subscribe')}}" method="post">
 @csrf
      <div class="modal-header">
        <h5 class="modal-title">Inscription à la newsletter</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      <div class="row">

              <div class="col-md-12">
                  <input type="text" class="form-control" name="name" id="name" placeholder="Votre nom.." required="">

              </div>
            
       </div>

        <div class="row">

              <div class="col-md-12">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Votre email.." required="">

              </div>
            
        </div>

       
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Valider</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <input type="hidden" name="idnewsletter" id="idnewsletter">
      </div>
       </form>
    </div>
  </div>
</div>

@endsection
