<form class="pl-3 pr-3 formcls" method="post" action="{{ route('admin.newsletter.store') }}">
    @csrf
 
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <label for="">*{{ __('Titre') }}</label>
                <input class="form-control" type="text" id="" name="titre" value=""  placeholder=""  >
            </div> 
          
         
        </div>
        <div class="row">
              <div class="col-md-4">
                <label for="">*{{ __('Description') }}</label>
                <textarea class="form-control" required="" name="description" placeholder="Description" ></textarea>   
            </div> 
        </div>
    </div>

 
    <div class="form-group tright">
        <button class="btn btn-primary" type="submit">{{ __('Créer ') }}</button>
    </div>
 
</form>
 
 