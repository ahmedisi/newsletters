@extends('layouts.layout-admin')
@section('page-title') {{__('Liste newsletters')}} @endsection

@section('content')


 <div class="subheader py-2 py-lg-4 subheader-solid   " id="kt_subheader">
              <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-2">
                  <!--begin::Page Title-->
                  <h5 class="text-dark font-weight-bold my-1 mr-5">{{__('Liste des clients')}}</h5>
                  <!--end::Page Title-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                  <!--begin::Dropdowns-->
                  <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="Créer un nouveau client">
                   
                   <button type="button" class="btn btn-primary btn-rounded " data-ajax-popup="true" data-size="lg" data-title="Créer une newsletter" data-url="{{route('admin.newsletter.create')}}">
                                <i class="mdi mdi-plus"></i> {{__('Créer ')}}
                   </button>
                
                  </div>
                </div>
                <!--end::Toolbar-->
              </div>
</div>
<div id="kt_datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer" style="margin-top: 100px">
<div class="row">
  <div class="col-md-12">
       <div class="card card-custom gutter-b">
      
              <!-- /.card-header -->
              <div class="card-body">
                <table  id="datatable" class="table table-separate datatable-client table-head-custom table-checkable dataTable no-footer dtr-inline mb-0 animated ">
                  <thead>
                  <tr>
                    
                    <th class="dt-left sorting_disabled"></th>
                    <th>{{ __('Titre')}}</th>
                    <th>{{ __('Description')}}</th>
                    <th>{{ __('Actions')}}</th>
                  </tr>
                  </thead>
                  <tbody>


                     @foreach ($newsletters as $val)
                    <tr>
                    <td class="dt-left sorting_disabled" rowspan="1" colspan="1" style="width: 35px;" aria-label=" ID">
                    <label class="checkbox checkbox-single">
                        <input type="checkbox" value="{{$val->id}}" class="group-checkable">
                        <span></span>
                    </label></td>
                    <td> {{$val->titre}}</td>
                    <td>{{$val->description}}</td>

                    <td>
                      <a href="#" class="btn btn-primary" id="sendmail" data="{{$val->id}}">
                          envoyer une newsletter  
                          <span style="display: none" id="loading" class="badge btn-danger">EN cours...</span>
                      </a>
                    </td>

                  </tr>

                     @endforeach
                    
   
                  
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
  </div>
</div>
</div>
 
 <input type="hidden" id="baseurl" value="{{route('admin.newsletter.send')}}">
@endsection

@push('scripts')

 
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatable_filter').append('<div class="btnremove btn-group"><input class="checkall" type="checkbox" onclick="checkAll(this)"  /> <button type="button"  onclick=\'(confirm("Êtes-vous sûr ?")?document.getElementById("delete-all").submit(): "");\' class="btn btn-danger btn-rounded " >Supprimer</button></div>')
  })
  
</script>
 

<script type="text/javascript">
	$(document).ready(function(){
		$('#datatable_filter').prepend('<div style="display: none;" class="btnfiltre btn-group"><button type="button" class="btn btn-primary btn-rounded " >Filtre</button></div>');
		$('#datatable_filter label').contents().filter(function() {
    return this.nodeType == 3
}).each(function(){
    this.textContent = '';
});
		
	})
	
</script>
@endpush