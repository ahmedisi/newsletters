<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear/to', function () {
  Artisan::call('cache:clear');
  Artisan::call('config:clear');
  Artisan::call('config:cache');
  Artisan::call('view:clear');
  return "Cleared!";
});



Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::post('/subscribe/', 'HomeController@subscribe')->name('newsletter.subscribe');

Route::prefix('admin')->as('admin.')->group(function() {
Route::get('/dashboard',['as' => 'dashbord','uses' =>'DashbordController@showForm'])->middleware(['auth']);
Route::get('/newsletter/create',['as' => 'newsletter.create','uses' =>'DashbordController@create'])->middleware(['auth']);
Route::post('/newsletter/store',['as' => 'newsletter.store','uses' =>'DashbordController@store'])->middleware(['auth']);

Route::post('/newsletter/send',['as' => 'newsletter.send','uses' =>'DashbordController@send'])->middleware(['auth']);

});

