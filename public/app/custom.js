var NEWSLETTERS= {};

( function(){
    /**
     *  
     **/
    NEWSLETTERS.sendnewsletters=function(){
        jQuery("#sendmail").on('click',function(){


        	  $('#loading').fadeIn('slow')

        	   $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


        	   $.ajax({
                url: $('#baseurl').val(),
                type: "post",
                data: { id: $(this).attr('data') },
                cache: false,
                success: function (data) {
console.log(data)
                	 if(data.success==1){
                	 	 $('#loading').fadeOut('slow');
                	 }
                	 else{
                	 	alert('Une erreur est survenue !')
                	 }
                    
                    
                }
            })

             
        });
    };

    /**
     *  
     **/
     NEWSLETTERS.getArticleId=function(docId){
        jQuery("#subscribbtn").on('click',function(){
             id=$(this).attr('data');
             $('#idnewsletter').val(id)
        });
     }

 

})();
/* -- end of NEWSLETTERS namespace -- */

/**
 *  how to get query param
 **/
(function($) {
    $.QueryString = (function(a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i)
        {
            var p=a[i].split('=');
            if (p.length != 2) continue;
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'))
})(jQuery);

 
//"onload"
jQuery(document).ready(function(){
    NEWSLETTERS.sendnewsletters();
    NEWSLETTERS.getArticleId();
    
    
});