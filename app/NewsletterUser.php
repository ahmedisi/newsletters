<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class NewsletterUser extends Model
{
    
    protected $guard = 'abonnements';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'newsletter_id'
    ];
    
 
 
}
