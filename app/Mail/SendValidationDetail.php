<?php

namespace App\Mail;

use App\User;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendValidationDetail extends Mailable
{
    use Queueable, SerializesModels;
     public $user;
     public $newsletter;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,$newsletter)
    {
        $this->user = $user;
        $this->newsletter = $newsletter;
 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.validation')->subject('Newsletter validation - '.env('APP_NAME'));
    }
}
