<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Newsletter;
use App\NewsletterUser;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendValidationDetail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the list newsletter.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $newsletters=Newsletter::get();
        $message='';

        return view('home',compact('newsletters','message'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function subscribe(Request $request)
    {
        
     $fields=[
                'name' => 'required',
                'email' => 'required|email',
                'idnewsletter' => 'required',
            ];
  
    $request->validate(
             $fields
        );



    $user=User::where('email',$request->email)->first();
 
    if(!$user and empty($user->id)){


    $arrUser['name']              = $request->name;
    $arrUser['email']              = $request->email;
    $arrUser['password']    = Hash::make('1234');
    $arrUser['gard']        = 'client';
    $user           = User::create($arrUser);

    $user->remember_token = $remember_token = Str::random(60);
    $user->save();      
    }

    $newsletters=Newsletter::find($request->idnewsletter);
    // save subscrib

    $issubscibed=NewsletterUser::where('user_id',$user->id)->where('newsletter_id',$request->idnewslette)->first();
 
     if(!$issubscibed or empty($issubscibed->id)){
         
        $arr['newsletter_id']              = $request->idnewsletter;
        $arr['user_id']              = $user->id;
        $NewsletterUser           = NewsletterUser::create($arr);


        try {

            Mail::to($user->email)->send(new SendValidationDetail($user,$newsletters->titre));
            
        } catch (\Exception $e) {
            
        }

        $message="<span class='badge badge-success'>Vous avez été inscrit avec succès à la Newsletter</span>";
         
     }
     else{
        
         $message="<span class='badge badge-danger'>Vous êtes déja abonné à cet newsletter.</span>";
     }

     $newsletters=Newsletter::get();

      return view('home',compact('newsletters','message'));
    
  
    }


}
