<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Newsletter;
use App\NewsletterUser;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendNewsLetter;


class DashbordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showForm()
    {

        
        $newsletters=array();

        $newsletters=Newsletter::get();

        return view('admin.addnewsletter',compact('newsletters'));
    }


    /**
     * Create newsletter.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {

        
        return view('admin.create',compact('newsletters'));
    }
   /**
     * store newsletter.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {

     $userObj = Auth::user();

     $fields=[
                'titre' => 'required',
                'description' => 'required',
               
            ];
  
    $request->validate(
             $fields
        );


    $arr['titre']              = $request->titre;
    $arr['description']              = $request->description;


    $register  = Newsletter::create($arr);

    if($register){
         return redirect()->back()->with('success', __('Newsletter crée avec succes !'));
    }
    else{
          return redirect()->back()->with('error', __('Une erreur est survenue !'));
    }
             

    }


   /**
     * send newsletter.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function send(Request $request)
    {
   

       $idnewsLetter= $request->id;

       $newsletter=Newsletter::find($idnewsLetter);

       if($newsletter){

         $users = User::select(['users.*','newsletter_users.newsletter_id'])
        ->join('newsletter_users', 'users.id', '=', 'newsletter_users.user_id')
        ->where('newsletter_users.newsletter_id', '=', $idnewsLetter)
        ->get();

        $listMail=array();

        foreach ($users as $key => $value) {
            
        try {
            
            Mail::to($value->email)->send(new SendNewsLetter($value,$newsletter->titre));
        } catch (\Exception $e) {
            
        }
        }
     
       }

       $resultat = [
            'success' => 1,
        ];
        return  response()->json($resultat);
     
    }

     

}
