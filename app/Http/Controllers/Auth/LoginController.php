<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
 

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

      
        $this->middleware('guest')->except('logout');
 
    }

    protected function authenticated(Request $request, $user)
    {


    
    if ( $user->gard == 'admin' ) {// do your magic here
 
    
        $request->session()->put('user_admin', 1);
       
        return redirect()->route('admin.dashbord');
    }
    else{
        
        // traitement des permissions avant la redirection !!!
        \Auth::logout();
        return redirect()->route('home')->with('error', __("Désolé, vous n'êtes pas autorisé à accéder à cette page !"));

    }

     
    }

     /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');


        if (Auth::attempt($credentials)) {
            // Authentication passed...
           
          
            return redirect()->intended('dashboard');
        }
    }


     /**
     * Show Client Login Form  
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return View
     */

    public function showClientLoginForm(Request $request,$slug,$lang = '')
    {

    
        
       $workspace=Workspace::where('slug', $slug)->first();
       if(!$workspace || empty($workspace) ){
         return redirect()->route('home');
       }
       $currantWorkspace = $workspace->id;

        if ($lang == '') {
            $lang = env('DEFAULT_LANG') ?? 'fr';
        }

        \App::setLocale($lang);

        if(!$currantWorkspace){return false;}
       
         $bd_data = Bdd::where('workspace_id', $currantWorkspace)->first();
        
 
        if(empty($bd_data)){
            return redirect()->route('home');
        }

        try {
            ConnectToDataBase($currantWorkspace);
        } catch (Exception $e) {
          return redirect()->route('home');
        }

        $ar_theme = Theme::get();
      
        $theme = array();
        foreach($ar_theme as $val)
            $theme[$val->meta_name] = $val->meta_value;

        if(empty($ar_theme)){
             $theme['WORKSPACE_LOGO'] = '';
             $theme['btn_primary'] = '';
             $theme['btn_success'] = '';
             $theme['btn_info'] = '';
             $theme['btn_danger'] = '';
             $theme['btn_warning'] = '';
             $theme['WORKSPACE_SIDE_BARE_COLOR'] = '';
        }
 
    
        if(!empty($theme['WORKSPACE_LOGO'])){
         $logo=$theme['WORKSPACE_LOGO'];
         $logo= asset('/public/img/workspaces/logo/'.$currantWorkspace.'/'.$logo) ;
         $theme['WORKSPACE_LOGO']=$logo;
        }
        else{
         $logo= asset('/public/storage/logo/logo.png') ;
         $theme['WORKSPACE_LOGO']=$logo;
        }

        $style="";

        if(!empty($theme['btn_primary'])){
            $style.=".btn-primary{box-shadow: 0 2px 6px #".$theme['btn_primary']." !important;background-color:".$theme['btn_primary']." !important;border-color:".$theme['btn_primary']." !important;}";
            $style.=".card-nis{border-top-color: ".$theme['btn_primary']." !important;}";
            $style.=".card .card-header h4{color: ".$theme['btn_primary']." !important;}";
        }


        $request->session()->put('theme', $theme);


        return view('auth.client_login', compact('lang','theme','currantWorkspace','logo','slug','style'));
    }
    
    public function clientLogin(Request $request,$slug)
    {
         
       $workspace=Workspace::where('slug', $slug)->first();
       $currantWorkspace = $workspace->id;
       if(!$currantWorkspace){return false;}

        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:4'
        ]);


        $permissions = Permission::get();

        // permission by id
        
        $permissions_ids=array();
        foreach ($permissions as $key => $value) {
            $permissions_ids[$value->id]=$value->slug;
        }


        $permissions_slug=array();
        foreach ($permissions as $key => $value) {
            $permissions_slug[]=$value->slug;
        }

        // 



       // if owner
        $firstconnectOwner=\Auth::attempt(['email' => $request->email, 'password' => $request->password, 'type' => 'client', 'currant_workspace' => $currantWorkspace], $request->get('remember'));

         $user=\Auth::user();
        if ($firstconnectOwner and empty($user->slug) ) {
            // get all permissions

             $request->session()->put('user_client', 1);
             $request->session()->put('permissions_slug', $permissions_slug);
             return redirect()->route('client.dashboard');
        }
        else {
        
         // if user

         $bd_data = Bdd::where('workspace_id', $currantWorkspace)->first();
         if(empty($bd_data)){
            return $this->sendFailedLoginResponse($request);
          }

          try {
            $user=\Auth::user();
            ConnectToDataBase($currantWorkspace);

              $isuser=User::where('email',$request->email)->first();
              if(empty($isuser)){
                  return $this->sendFailedLoginResponse($request);
              }

              $roles=UserWorkspace::where('user_id','=',$isuser->id)->first();
              $roles=$roles->roles;

              $roles=json_decode($roles,true);
              $permissions_slug=array();
              foreach ($roles as $key => $role) {
                $objRole=Role::find($role);
                 if($objRole){
                   $permissions=$objRole->getPermissions();  
                   if($permissions){
                    $permissions=json_decode($permissions,true);
                    foreach ($permissions as $key => $value) {
                         $permissions_slug[]=$permissions_ids[$value];
                    }  
                   }
                 }
            }
   
             $request->session()->put('user_client', 1);
             $request->session()->put('permissions_slug', $permissions_slug);

             
             return redirect()->route('client.dashboard');
          

         } catch (Exception $e) {
           return $this->sendFailedLoginResponse($request);
         }


        }
        return $this->sendFailedLoginResponse($request);
    }


    public function checktoken(Request $request,$token)
    {



        $user = User::where('token',$token)->first();
        
        if(!empty($user)){
            
            $update=[];
            $update['email_verified_at']=date('Y-m-d H:m:s');
            User::where('token',$token)->update($update);
            return redirect()->route('login')->with('success', __("Votre compte est vérifié !"));

        }
        else{
           return redirect()->route('login')->with('error', __("Votre compte n'est pas vérifié !"));
        }

         
         
    }



    public function showLoginForm($lang = '')
    {
        if ($lang == '') {
            $lang = env('DEFAULT_LANG') ?? 'fr';
        }

        \App::setLocale($lang);

        return view('auth.login', compact('lang'));
    }

    public function accountnotactivate()
    {
        
         

        \App::setLocale('fr');

        return view('auth.accountnotactivate');
    }


}
