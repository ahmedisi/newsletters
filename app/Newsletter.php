<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    
    protected $guard = 'newsletters';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titre', 'description'
    ];
    
    /**
     * The users that belong to the role.
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
 
}
