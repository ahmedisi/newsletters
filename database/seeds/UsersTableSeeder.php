<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminUser = User::create([
              'name' => 'Admin',
              'email' => 'admin@example.com',
              'password' => \Illuminate\Support\Facades\Hash::make('1234'),
              'gard' => 'admin'
          ]);
    }
}
